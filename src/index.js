import Todo from './todo/todo.js';

import { header } from './utils/header';
import { todoNewItemForm } from './utils/todo-new-item-form.js';
import { createTable } from './utils/create-table';

import './style.css';

const section = document.createElement('section');
section.classList.add('todoapp');

section.appendChild(header('todo'));

const todoSection = document.createElement('section');

todoSection.appendChild(todoNewItemForm());
todoSection.appendChild(createTable('unarchived', true));
todoSection.appendChild(createTable('archived', true));


section.appendChild(todoSection);
document.body.appendChild(section);