import TodoItem from './todo-item.js';

const Todo = {
    todoItems: [],

    addTodo(text) {
        const item = new TodoItem(this.todoItems.length + 1, false, text, false);
        this.todoItems.push(item);
        return item;
    },

    getTodoById(id) {
        return this.todoItems.find(todo => todo.id === id);
    },

    earchTodoItems(query) {
        return this.todoItems.filter(todo => !todo.isDeleted && todo.text.toLowerCase().indexOf(query.toLowerCase()) !== -1);
    },

    toggleTodoCompleted(id) {
        let todo = this.getTodoById(id);
        if (todo) {
            todo.isCompleted = !todo.isCompleted;
        }
    },

    toggleDelete(id) {
        let todo = this.getTodoById(id);
        if (todo) {
            todo.isDeleted = !todo.isDeleted;
        }
    },

    getOnlyDeletedTodoItems() {
        return this.todoItems.filter(todo => todo.isDeleted);
    },

    getOnlyUndeletedTodoItems() {
        return this.todoItems.filter(todo => !todo.isDeleted);
    },

    updateTodoText(text, id) {
        let todo = this.getTodoById(id);
        if (todo) {
            todo.text = text;
        }
    }
};

module.exports = Todo;