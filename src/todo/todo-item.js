const TodoItem = function (id, isCompleted, text, isDeleted, createdAt) {
    let _id = id;
    let _isCompleted = isCompleted;
    let _text = text;
    let _isDeleted = isDeleted;
    let _createdAt = createdAt || new Date();

    Object.defineProperty(this, 'isCompleted', {
        get() { return _isCompleted; },
        set(value) { _isCompleted = value; },
        configurable: false
    });

    Object.defineProperty(this, 'text', {
        get() { return _text; },
        set(value) { _text = value; },
        configurable: false
    });

    Object.defineProperty(this, 'isDeleted', {
        get() { return _isDeleted; },
        set(value) { _isDeleted = value; },
        configurable: false
    });

    Object.defineProperty(this, 'createdAt', {
        get() { return _createdAt; },
        configurable: false
    });

    Object.defineProperty(this, 'id', {
        get() { return _id; },
        configurable: false
    });
};

let defineProperty = function (context, propertyName, funcProperty, isConfigurable) {
    Object.defineProperty(this, propertyName, {
        get() { return funcProperty; },
        set(value) { funcProperty = value; },
        configurable: isConfigurable
    });
};

module.exports = TodoItem;