import Todo from '../todo/todo.js';

import { todoArchivedRowItem } from './todo-archived-row-item';
import { addToTodoTable } from './add-to-todo-table';

export function addToArchivedTable(todoItem) {
    let table = document.getElementById('archived');
    let tbody = table.getElementsByTagName('tbody')[0];
    tbody.appendChild(todoArchivedRowItem(todoItem, (id) => {
        if (!isNaN(id)) {
            activateTodo(parseInt(id));
        }
    }));
};

let activateTodo = function (id) {
    let todoItem = Todo.getTodoById(id);
    todoItem.isDeleted = false;
    todoItem.isCompleted = false;
    addToTodoTable(todoItem);
};