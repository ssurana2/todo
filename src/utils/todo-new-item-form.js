import Todo from '../todo/todo.js';

import { addToTodoTable } from './add-to-todo-table';

export function todoNewItemForm() {
    let todoForm = document.createElement('form');
    todoForm.addEventListener('submit', (event) => {
        event.preventDefault();
        event.stopImmediatePropagation();
        onSubmit.call(this, event.target.elements[0]);
    });
    todoForm.appendChild(todoItemInput());
    return todoForm;
}

let todoItemInput = function () {
    let newTodoItemInput = document.createElement('input');
    newTodoItemInput.type = 'text';
    newTodoItemInput.classList.add('todo-item-input');
    newTodoItemInput.placeholder = 'What needs to be done?'
    newTodoItemInput.autofocus = true;
    newTodoItemInput.required = true;
    newTodoItemInput.maxLength = 120;
    return newTodoItemInput;
}

let onSubmit = function (element) {
    if (Todo.todoItems.length < 10) {
        let todoItem = Todo.addTodo(element.value);
        addToTodoTable(todoItem);
        element.value = '';
    } else {
        if (!document.getElementsByClassName('error-message').length) {
            let span = document.createElement('span');
            span.classList.add('error-message');
            span.innerHTML = 'Cannot add more than 10 Todos';
            element.closest('form').appendChild(span);
        }
    }
}