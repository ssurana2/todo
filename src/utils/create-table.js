export function createTable(id, show) {
    const table = document.createElement('table');
    table.id = id;
    table.classList.add(show ? 'show' : 'hide');
    const tbody = document.createElement('tbody');
    table.appendChild(tbody);
    return table;
}