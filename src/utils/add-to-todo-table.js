import Todo from '../todo/todo.js';
import { todoItemRow } from './todo-item-row';
import { addToArchivedTable } from './add-to-archived-table';

export function addToTodoTable(todoItem) {
    let table = document.getElementById('unarchived');
    let tbody = table.getElementsByTagName('tbody')[0];
    tbody.appendChild(todoItemRow(todoItem, (event) => {
        Todo.toggleTodoCompleted(parseInt(event.target.id));
        let parent = event.target.closest('tr');
        let text = parent.getElementsByClassName('text')[0];
        if (text.classList.contains('line-through')) {
            text.classList.remove('line-through');
        } else {
            text.classList.add('line-through');
        }
    }, (text, id) => {
        if (!isNaN(id)) {
            Todo.updateTodoText(text, parseInt(id));
        }
    }, (id) => {
        if (!isNaN(id)) {
            Todo.toggleDelete(parseInt(id));
            let todoItem = Todo.getTodoById(parseInt(id));
            addToArchivedTable(todoItem);
        }
    }));
};