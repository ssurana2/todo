export function todoArchivedRowItem(todoItem, onActivate) {
    if (todoItem.isDeleted) {
        let tr = document.createElement('tr');
        tr.setAttribute('data-id', todoItem.id);
        tr.appendChild(addTextToTd(todoItem.text));
        tr.appendChild(activateButton(tr, onActivate));
        return tr;
    }
};

let addTextToTd = function (text) {
    let td = document.createElement('td');
    td.classList.add('text');
    td.innerHTML = text;
    return td;
};

let activateButton = function (tr, onActivate) {
    let td = document.createElement('td');
    let button = document.createElement('button');
    button.innerHTML = 'Put Back';
    button.classList.add('default');
    button.addEventListener('click', (event) => {
        let parent = tr;
        onActivate.call(this, parent.getAttribute('data-id'));
        parent.remove();
    });
    td.appendChild(button);
    return td;
};