export function header(text) {
    let header = document.createElement('header');
    let h1 = document.createElement('h1');
    h1.innerHTML = text;
    header.appendChild(h1);
    return header;
}