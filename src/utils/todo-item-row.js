export function todoItemRow(todoItem, onChange, onFormSubmit, onDelete) {
    let tr = document.createElement('tr');
    tr.setAttribute('data-id', todoItem.id);
    tr.appendChild(checkboxTd(todoItem.isCompleted, todoItem.id, onChange));
    tr.appendChild(textTd(todoItem.text, todoItem.id, tr, onFormSubmit));
    tr.appendChild(buttonsTd(onDelete));
    return tr;
};

let checkboxTd = function (isCompleted, id, onChange) {
    let td = document.createElement('td');
    td.classList.add('checkbox');
    td.appendChild(getCheckbox(isCompleted, id, onChange));
    let label = document.createElement('label');
    label.setAttribute('for', id);
    td.appendChild(label);
    return td;
};

let getCheckbox = function (isCompleted, id, onChange) {
    let checkbox = document.createElement('input');
    checkbox.type = 'checkbox';
    checkbox.checked = isCompleted;
    checkbox.name = id;
    checkbox.id = id;
    checkbox.addEventListener('change', function (event) {
        onChange.call(this, event);
    });
    return checkbox;
};

let textTd = function (text, id, tr, onFormSubmit) {
    let td = document.createElement('td');
    td.classList.add('text');
    td.appendChild(divText(text, id, td, onFormSubmit));
    return td;
};

let divText = function (text, id, td, onFormSubmit) {
    let span = document.createElement('div');
    span.innerHTML = text;
    span.addEventListener('dblclick', (event) => {
        let innerText = event.target.innerText;
        span.remove();
        td.appendChild(formTextInput(innerText, id, td, onFormSubmit));
    });
    return span;
}

let formTextInput = function (text, id, td, onFormSubmit) {
    let form = document.createElement('form');
    form.classList.add('form-inline');
    form.addEventListener('submit', function (event) {
        event.preventDefault();
        event.stopImmediatePropagation();
        let text = event.target.elements[0].value;
        onFormSubmit.call(this, text, id);
        form.remove();
        td.appendChild(divText(text, id, td, onFormSubmit));
    });
    let input = document.createElement('input');
    input.classList.add('todo-input');
    input.type = 'text';
    input.placeholder = 'Todo item';
    input.required = true;
    input.maxLength = 120;
    input.value = text;
    form.appendChild(input);
    return form;
}

let buttonsTd = function (onDelete) {
    let td = document.createElement('td');
    td.classList.add('buttons-group');
    td.appendChild(deleteButton(onDelete));
    return td;
};

let deleteButton = function (onDelete) {
    let button = document.createElement('button');
    button.innerHTML = 'Delete';
    button.classList.add('danger');
    button.addEventListener('click', (event) => {
        let parent = event.target.closest('tr');
        onDelete.call(this, parent.getAttribute('data-id'));
        parent.remove();
    });
    return button;
};