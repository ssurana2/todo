# TODO #

A TODO application built using ECMA Script 2015, vanilla Javascript and webpack

### How do I get set up? ###

* Install the latest Node.
* Run `npm install -g webpack` to install the latest webpack module globally on your local
* Clone the todo repo
* Navigate to the todo repo and run `npm install`
* Run `npm run build` for building the todo application. The build application would be stored in `dist` folder
* Run `npm run start` for starting the development server